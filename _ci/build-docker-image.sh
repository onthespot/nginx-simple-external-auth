#!/bin/sh

set -eu

docker build -t app:${CI_COMMIT_SHA:0:8} .

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com

docker tag app:${CI_COMMIT_SHA:0:8} ${GITLAB_DOCKER_IMAGE}:${CI_COMMIT_SHA:0:8}
docker tag app:${CI_COMMIT_SHA:0:8} ${GITLAB_DOCKER_IMAGE}:${CI_COMMIT_REF_NAME}

docker push ${GITLAB_DOCKER_IMAGE}:${CI_COMMIT_SHA:0:8}
docker push ${GITLAB_DOCKER_IMAGE}:${CI_COMMIT_REF_NAME}