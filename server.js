function randomString() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

const express = require('express')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

const app = express()

const TOKEN = process.env.TOKEN;
const SECRET = randomString();
const DATA = randomString();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser())

app.get('/__auth/login', function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.writeHead(200);
    res.end("<html><body><form method='post'><input type='password' name='pwd'><input type='submit'></form></body></html>");
})

app.post('/__auth/login', function (req, res) {
    if (TOKEN == req.body.pwd) {
        var token = jwt.sign({ data: DATA, nonce: randomString() }, SECRET, { expiresIn: '8h' });
        res.cookie("_token", token)
        res.redirect('/');
    } else {
        res.redirect('/__auth/login')
    }
})

app.all('*', function (req, res) {
    try {
        var decoded = jwt.verify(req.cookies["_token"], SECRET);
        if (DATA == decoded.data) {
            res.send('OK')
        } else {
            res.send(401)
        }
    } catch (err) {
        res.send(401)
    }
})

app.listen(3000, '0.0.0.0');